CREATE TABLE `movies` (
  `id_movie` int NOT NULL AUTO_INCREMENT,
  `Movie_title` varchar(100) NOT NULL,
  `Actor_name` varchar(100) NOT NULL,
  `Vote` int DEFAULT NULL,
  `Year` int NOT NULL,
  `Intro` varchar(350) NOT NULL,
  `Plot` text NOT NULL,
  `Link_Yt` varchar(150) NOT NULL,
  `Link_Wk` varchar(150) DEFAULT NULL,
  `Link_Poster` varchar(150) DEFAULT NULL,
  `Link_Actor_img` varchar(150) DEFAULT NULL,
  `Actor_year` date NOT NULL,
  `Actor_bio` varchar(350) NOT NULL,
  PRIMARY KEY (`id_movie`),
  UNIQUE KEY `id_movies_UNIQUE` (`id_movie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
